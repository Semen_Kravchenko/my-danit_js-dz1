//Ответы на теорию:

//1 вопрос: Обьяснить своими словами разницу между обьявлением переменных через var, let и const.

//let и const ведут себя одинаково, за исключением того, что мы не можем изменить значение, которое ранее уже было объявлено в const, а в let можем.

//В отличии от var, переменную, созданную с помощью let/const, нельзя объявить дважды:
//=================================================
//var x = 5;
//var x = 10;
//console.log(x); - выведет 10
//=================================================
//let(const) x = 5;
//let(const) x = 10;
//console.log(x); - выведет ошибку(undeclared)
//=================================================

//Следующее отличие var от let/const - это всплытие переменных, когда над переменной производится операция до её объявление. Всплытие возможно с переменными, объявленными только с помощью var.
//=================================================
//x = 10;
//console.log(x); - выведет 10
//var x;

//либо

//console.log(x); - выведет undefined, так как посчитает переменную x пустой.
//x = 10;
//var x;
//================================================
//x = 10;
//console.log(x); - в этой, и другой ситуации выведет ошибку
//let(const) x;
//================================================

//Последним отличием var от let/const будет - область видимости.
// Если переменная заданная с помощью var, то она будет видна, как глобальная, в отличии от let/const, которые имеют область видимости только в рамках своего блока ({}).

//Если объявить глобальную переменную с помощью var, а потом объявить такую же переменную внутри другого блока, то мы её просто переинициализируем.
//В случае с let/const, переменная, объявленна глобально и та, которая объявленна локально - две разные переменные.
//================================================
//var x = 5;
//console.log(x); - выведет 5
//  {
//      var x = 10;
//      console.log(x); - выведет 10
//  }
//console.log(x); - выведет 10
//================================================
//let(const) x = 5;
//console.log(x); - выведет 5
//  {
//      let(const) x = 10;
//      console.log(x); - выведет 10
//  }
//console.log(x); - выведет 5

//2 вопрос: Почему объявлять переменную через var считается плохим тоном?

//Ввиду всех выше сказанных причин... в особенности это касается такого недостатка var, как область видимости, ибо блочная область видимости это зэр гуд!

//Задание:

"use strict";

let userName;
let userAge;
let userAgree;

do {
    userName = prompt(`Enter your name: `, userName);
    userAge = +prompt(`Enter your age: `, userAge);

} while (userName === "" ||
        userName === null ||
        userName === undefined ||
        isNaN(userAge) ||
        userAge === "" ||
        userAge === null ||
        userAge === undefined);

if (userAge < 18) {
    alert(`You are not allowed to visit this website`);
}
else if (userAge >= 18 && userAge <= 22) {
    userAgree = confirm(`Are you sure you want to continue?`);
    if (userAgree === true) {
        alert(`Welcome, ${userName}`);
    }
    else {
        alert(`You are not allowed to visit this website`);
    }
}
else {
    alert(`Welcome, ${userName}`);
}
